# Manual Técnico 💻



## Clase Main :

En esta clase se genero el menú principal de esta aplicación la cual fue creada usando el lenguaja C++.

[![Captura-de-pantalla-2023-08-30-230952.png](https://i.postimg.cc/LXQF4HGv/Captura-de-pantalla-2023-08-30-230952.png)](https://postimg.cc/r04PQLVt)

Para esta aplicación se necesito crear diversas clases las cuales tiene diversas funcionalidades, entre las clases que se crearon estan las siguiente:
- ListaEmpleados:
    * Esta clase se creo con la finalidad de crear una lista doblemente enlazada la cual contenga la información de los empleados, para almacenar la información separada de los empleados se utilizo una clase llamada {+ NodoEmpleados +}, dicha clase fue de ayuda para poder guardar la información de los empleados de manera indiviudal.

    [![Captura-de-pantalla-2023-08-30-234029.png](https://i.postimg.cc/3w8n9zwH/Captura-de-pantalla-2023-08-30-234029.png)](https://postimg.cc/v4KrZq9P)
    
    * Como se puede notar en esta clase se utilizaron funciones como: 
        + CargaMasiva(std::string archivo: Esta fue utilizada para leer un archivo cvs y extraer todo los datos de nombre y contraseña de cada uno de los empleados.
        + Mostrar(): Es usada para mostrar los empleados en el sistema.
        + InsertarEmpleado(): Utilizada para guardar información de un nuevo empleado.
        + Login(): Esta función es utilizada para verificar que el usurio de Proyect Manager existe y así acceder a cada una de las funciones del programa.

- ColaPrioridad:
    * En está clase se utilizo para guardar la infomación de los proyectos, esta clase funciono como lista con un comportamiento de cola en el cual se fue ordenando de acuerdo a la prioridad que tiene cada proyecto. Para guardar la información de manera ordenada se utilizo la {+ NodoCola +} la cual almaceno en cada objeto creado la información acerca del proyecto en cuestión.

    [![Captura-de-pantalla-2023-08-30-235041.png](https://i.postimg.cc/RVr702KL/Captura-de-pantalla-2023-08-30-235041.png)](https://postimg.cc/mhwzXXdP)

    * Como se puede notar en esta clase se utilizaron funciones como: 
        + Encolar(std::string Nombre, std::string Tipo_de_Prioridad): Esta función fue utilizada para guardar la información de los proyectos.
        + Descolar(): Esta función fue utilizada para vaciar la lista al momento de crear las columnas en la matriz dispersa.
        + VerProyecto(): Esta función es utilizada para ver la información de los proyectos.
        + Ordenar(): Esta función se utilizo para ordenar los nodos de la lista de acuerdo a la importancia que tenía el proyecto que guardaba.
        + GraficarCola(): Esta función se utilizo para crear un reporte en el cual se puediera visualizar por medio de una imagen el orden de los proyectos.

- Matriz


